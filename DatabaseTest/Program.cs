﻿namespace DatabaseTest
{
    using System;
    using Setup;
    using Unity;

    class Program
    {
        //Тут говнокодит, просто чтобы дернуть методы clientTypeManager и tokenManager
        static void Main(string[] args)
        {
            var container = new UnityContainer();
            DependencyInjectionMapper.Map(container);

            var applicationClientManager = new ApplicationClientManager(container);
            var tokenManager = new TokenManager(container);

            Console.WriteLine("Options:");
            Console.WriteLine("  1 - Show all client types");
            Console.WriteLine("  2 - Get client types by id");
            Console.WriteLine("  3 - Show all tokens");
            Console.WriteLine("  4 - Add new token");

            while (true)
            {
                Console.Write("Select option: ");
                var readKey = Console.ReadLine();

                var isNumberValid = int.TryParse(readKey, out var result);
                if (!isNumberValid)
                {
                    Console.WriteLine("Invalid input!");
                }
                else
                {
                    switch (result)
                    {
                        case 1:
                            Console.WriteLine("Working...");
                            applicationClientManager.ShowAll().Wait(); //Wait - async/await Попробуй-те убрать, посмотрите как измениться вывод)
                            Console.WriteLine("Done");
                            break;

                        case 2:
                            Console.Write("Client type id: ");
                            var idText = Console.ReadLine();
                            var isIdValid = int.TryParse(idText, out var id);
                            if (!isIdValid)
                            {
                                Console.WriteLine("Invalid input!");
                            }
                            Console.WriteLine("Working...");
                            applicationClientManager.ShowById(id).Wait(); //Wait - async/await Попробуй-те убрать, посмотрите как измениться вывод)
                            Console.WriteLine("Done");
                            break;

                        case 3:
                            Console.WriteLine("Working...");
                            tokenManager.ShowAll().Wait(); //Wait - async/await Попробуй-те убрать, посмотрите как измениться вывод)
                            Console.WriteLine("Done");
                            break;

                        case 4:
                            Console.Write("Token value: ");
                            var tokenValue = Console.ReadLine();
                            Console.Write("Client type id: ");
                            var clientTypeIdText = Console.ReadLine();
                            var isClientTypeIdValid = int.TryParse(clientTypeIdText, out var clientTypeId);
                            if (!isClientTypeIdValid)
                            {
                                Console.WriteLine("Invalid input!");
                            }
                            Console.WriteLine("Working...");
                            tokenManager.AddToken(tokenValue, clientTypeId).Wait();
                            Console.WriteLine("Done");
                            break;

                        default:
                            Console.WriteLine("Wrong option");
                            break;
                    }
                }
            }
        }
    }
}