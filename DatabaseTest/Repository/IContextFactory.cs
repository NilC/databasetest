﻿namespace DatabaseTest.Repository
{
    using Context;

    public interface IContextFactory
    {
        IDatabaseContext CreateDatabaseContext();
    }
}