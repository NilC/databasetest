﻿namespace DatabaseTest.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;

    public interface IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        TEntity Add(TEntity entity);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        Task<List<TEntity>> GetAllAsync();
        IQueryable<TEntity> GetAllAsQueryable();
        IQueryable<TEntity> GetAllAsNoTrackingQueryable();

        TEntity GetExistingOrAttach(TEntity entity);

        void Update(TEntity entity);
    }
}