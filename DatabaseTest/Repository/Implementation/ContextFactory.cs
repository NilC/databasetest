﻿namespace DatabaseTest.Repository.Implementation
{
    using Context;
    using Context.Implementation;
    using Repository;

    public class ContextFactory : IContextFactory
    {
        public IDatabaseContext CreateDatabaseContext()
        {
            return new DatabaseContext();
        }
    }
}