﻿namespace DatabaseTest.Repository.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using Context;
    using Models;
    using Repository;
    using Unity;
    using Unity.Resolution;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IUnityContainer _unityContainer;

        private readonly IDatabaseContext _context;
        private readonly Dictionary<Type, object> _repositories;

        public UnitOfWork(IUnityContainer unityContainer)
        {
            Debug.WriteLine("UnitOfWork.Create");

            _unityContainer = unityContainer;

            _context = _unityContainer.Resolve<IContextFactory>().CreateDatabaseContext();
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<TId, TEntity> GetRepository<TId, TEntity>() where TEntity : class, IIdentityEntity<TId> where TId : struct
        {
            if (_repositories.Keys.Contains(typeof(TEntity)))
            {
                return _repositories[typeof(TEntity)] as IRepository<TId, TEntity>;
            }

            var repository = _unityContainer.Resolve<IRepository<TId, TEntity>>(new ParameterOverride("context", _context));
            _repositories.Add(typeof(TEntity), repository);

            return repository;
        }

        public Task<int> CommitAsync(Guid? userAccountId = null)
        {
            Debug.WriteLine("UnitOfWork.CommitAsync");
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Debug.WriteLine("UnitOfWork.Dispose");
            _context?.Dispose();
        }
    }
}