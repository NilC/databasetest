﻿namespace DatabaseTest.Repository.Implementation
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Context;
    using Models;

    public class Repository<TId, TEntity> : IRepository<TId, TEntity> where TEntity : class, IIdentityEntity<TId> where TId : struct
    {
        private readonly IDatabaseContext _context;

        public Repository(IDatabaseContext context)
        {
            _context = context;
        }

        public TEntity Add(TEntity entity)
        {
            return _context.Set<TEntity>().Add(entity);
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            return _context.Set<TEntity>().AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().RemoveRange(entities);
        }

        public Task<List<TEntity>> GetAllAsync()
        {
            return _context.Set<TEntity>().ToListAsync();
        }

        public IQueryable<TEntity> GetAllAsQueryable()
        {
            return _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAllAsNoTrackingQueryable()
        {
            return _context.Set<TEntity>().AsNoTracking();
        }

        public TEntity GetExistingOrAttach(TEntity entity)
        {
            var dbEntityEntry = _context.ChangeTracker
                .Entries<TEntity>()
                .FirstOrDefault(x => x.Entity.Id.Equals(entity.Id));

            if (dbEntityEntry != null)
                return dbEntityEntry.Entity;

            Attach(entity);

            return entity;
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
        }

        private void Attach(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Unchanged;
        }
    }
}