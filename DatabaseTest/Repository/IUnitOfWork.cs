﻿namespace DatabaseTest.Repository
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IUnitOfWork : IDisposable
    {
        IRepository<TId, TEntity> GetRepository<TId, TEntity>() where TEntity : class, IIdentityEntity<TId> where TId : struct;
        Task<int> CommitAsync(Guid? userAccountId = null);
    }
}