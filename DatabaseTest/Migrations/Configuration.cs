namespace DatabaseTest.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using Context.Implementation;
    using Models.Entities.Common;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DatabaseContext context)
        {
            var clientTypes = new List<ApplicationClient>
            {
                new ApplicationClient { Id = 1, Name = "Web" },
                new ApplicationClient { Id = 2, Name = "iOS" },
                new ApplicationClient { Id = 3, Name = "Android" },
            };
            clientTypes.ForEach(x => context.ClientTypes.AddOrUpdate(y => y.Id, x));
            context.SaveChanges();
        }
    }
}