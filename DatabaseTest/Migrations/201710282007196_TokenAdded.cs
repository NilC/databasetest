namespace DatabaseTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TokenAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.authToken",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Value = c.String(nullable: false),
                        ClientTypeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.comClientType", t => t.ClientTypeId)
                .Index(t => t.ClientTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.authToken", "ClientTypeId", "dbo.comClientType");
            DropIndex("dbo.authToken", new[] { "ClientTypeId" });
            DropTable("dbo.authToken");
        }
    }
}
