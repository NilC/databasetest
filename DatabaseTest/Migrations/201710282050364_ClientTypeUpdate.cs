namespace DatabaseTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientTypeUpdate : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.comClientType", newName: "comApplicationClient");
            RenameColumn(table: "dbo.authToken", name: "ClientTypeId", newName: "ApplicationClientId");
            RenameIndex(table: "dbo.authToken", name: "IX_ClientTypeId", newName: "IX_ApplicationClientId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.authToken", name: "IX_ApplicationClientId", newName: "IX_ClientTypeId");
            RenameColumn(table: "dbo.authToken", name: "ApplicationClientId", newName: "ClientTypeId");
            RenameTable(name: "dbo.comApplicationClient", newName: "comClientType");
        }
    }
}
