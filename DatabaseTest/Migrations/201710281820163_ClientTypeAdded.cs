namespace DatabaseTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientTypeAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.comClientType",
                c => new
                    {
                        Id = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.comClientType");
        }
    }
}
