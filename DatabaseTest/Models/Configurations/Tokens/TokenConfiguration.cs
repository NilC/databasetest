﻿namespace DatabaseTest.Models.Configurations.Tokens
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using Entities.Tokens;

    class TokenConfiguration : EntityTypeConfiguration<Token>
    {
        public TokenConfiguration()
        {
            ToTable("authToken"); //Название таблицы

            HasKey(a => a.Id); //Ключевое поле
            Property(a => a.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity); //Генерировать Guid при добавлении новой записи

            Property(x => x.Value)
                .IsRequired(); //Поле обязательное

            HasRequired(x => x.ApplicationClient)
                .WithMany()
                .HasForeignKey(x => x.ApplicationClientId)
                .WillCascadeOnDelete(false); // false - чтобы при удалении из ClientType не удалялись записи из Token
        }
    }
}