﻿namespace DatabaseTest.Models.Configurations.Common
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using Entities.Common;

    public class ApplicationClientConfiguration : EntityTypeConfiguration<ApplicationClient>
    {
        public ApplicationClientConfiguration()
        {
            ToTable("comApplicationClient"); //Название таблицы

            HasKey(a => a.Id); //Ключевое поле
            Property(a => a.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None); //*НЕ* генерировать id при добавлении новой записи

            Property(x => x.Name)
                .IsRequired() //Поле обязательное
                .HasMaxLength(255); //Максимальный размер - 255 символов
        }
    }
}