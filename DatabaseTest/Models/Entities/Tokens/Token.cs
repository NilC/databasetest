﻿namespace DatabaseTest.Models.Entities.Tokens
{
    using Base;
    using Common;

    public class Token : DataEntity
    {
        public string Value { get; set; }
        public long ApplicationClientId { get; set; }
        public ApplicationClient ApplicationClient { get; set; } //Чтобы сгенерировать первичный ключ
    }
}