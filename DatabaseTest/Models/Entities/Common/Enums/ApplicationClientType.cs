﻿namespace DatabaseTest.Models.Entities.Common.Enums
{
    public enum ApplicationClientType : long
    {
        Web = 1,
        iOS = 2,
        Android = 3
    }
}