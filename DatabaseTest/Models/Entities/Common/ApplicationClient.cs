﻿namespace DatabaseTest.Models.Entities.Common
{
    using Base;

    public class ApplicationClient : LookupEntity
    {
        public string Name { get; set; }
    }
}