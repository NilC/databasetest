﻿namespace DatabaseTest.Models.Entities.Base
{
    public class LookupEntity : IIdentityEntity<long>
    {
        public long Id { get; set; }
    }
}