﻿namespace DatabaseTest.Models.Entities.Base
{
    using System;

    public class DataEntity : IIdentityEntity<Guid>
    {
        public Guid Id { get; set; }
    }
}