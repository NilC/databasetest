﻿namespace DatabaseTest
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Models.Entities.Common;
    using Repository;
    using Unity;

    public class ApplicationClientManager
    {
        private UnityContainer _unityContainer;

        public ApplicationClientManager(UnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        public async Task ShowAll()
        {
            try
            {
                using (var unitOfWork = _unityContainer.Resolve<IUnitOfWork>())
                {
                    var clientTypeRepository = unitOfWork.GetRepository<long, ApplicationClient>(); //long - тим Id

                    var allClientType = await clientTypeRepository
                        .GetAllAsQueryable()
                        .ToListAsync();

                    foreach (var clientType in allClientType)
                    {
                        Console.WriteLine($"{clientType.Id} - {clientType.Name}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                throw;
            }
        }

        public async Task ShowById(long id)
        {
            try
            {
                using (var unitOfWork = _unityContainer.Resolve<IUnitOfWork>())
                {
                    var clientTypeRepository = unitOfWork.GetRepository<long, ApplicationClient>(); //long - тим Id

                    var clientTypeWithId = await clientTypeRepository
                        .GetAllAsQueryable()
                        .Where(x => x.Id == id)
                        .ToListAsync();

                    Console.WriteLine($"With Id {id}:");

                    foreach (var clientType in clientTypeWithId)
                    {
                        Console.WriteLine($"{clientType.Id} - {clientType.Name}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                throw;
            }
        }
    }
}