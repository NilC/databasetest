﻿namespace DatabaseTest.Context
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Threading.Tasks;

    public interface IDatabaseContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbChangeTracker ChangeTracker { get; }
        DbEntityEntry Entry(object entity);
        Task<int> SaveChangesAsync();
    }
}