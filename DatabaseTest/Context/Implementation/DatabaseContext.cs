﻿namespace DatabaseTest.Context.Implementation
{
    using System.Data.Entity;
    using Models.Entities.Common;

    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DbSet<ApplicationClient> ClientTypes { get; set; }

        public DatabaseContext() :
            base("Database")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Database.SetInitializer<DatabaseContext>(null);

            modelBuilder.Configurations.AddFromAssembly(typeof(DatabaseContext).Assembly);
        }
    }
}