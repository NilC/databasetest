﻿namespace DatabaseTest.Setup
{
    using Repository;
    using Repository.Implementation;
    using Unity;

    public static class DependencyInjectionMapper
    {
        public static void Map(IUnityContainer container)
        {
            container.RegisterInstance(container);
            MapDataAccess(container);
        }

        private static void MapDataAccess(IUnityContainer container)
        {
            container.RegisterType<IContextFactory, ContextFactory>();
            container.RegisterType(typeof(IRepository<,>), typeof(Repository<,>));
            container.RegisterType<IUnitOfWork, UnitOfWork>();
        }
    }
}