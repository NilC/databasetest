﻿namespace DatabaseTest
{
    using System;
    using System.Data.Entity;
    using System.Threading.Tasks;
    using Models.Entities.Tokens;
    using Repository;
    using Unity;

    public class TokenManager
    {
        private UnityContainer _unityContainer;

        public TokenManager(UnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        public async Task AddToken(string value, long clientTypeId)
        {
            try
            {
                using (var unitOfWork = _unityContainer.Resolve<IUnitOfWork>())
                {
                    var tokensRepository = unitOfWork.GetRepository<Guid, Token>(); //guid - тим Id

                    var token = new Token
                    {
                        Id = Guid.NewGuid(), //генерирование Id без использования БД
                        Value = value,
                        ApplicationClientId = clientTypeId
                    }; 
                    tokensRepository.Add(token);

                    await unitOfWork.CommitAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                throw;
            }
        }

        public async Task ShowAll()
        {
            try
            {
                using (var unitOfWork = _unityContainer.Resolve<IUnitOfWork>())
                {
                    var tokensRepository = unitOfWork.GetRepository<Guid, Token>(); //long - тим Id

                    var tokens = await tokensRepository
                        .GetAllAsQueryable()
                        .Include(x => x.ApplicationClient) //Вытаскиваем из базы еще и ClientType, чтобы тут же использовать имя
                        .ToListAsync();

                    foreach (var token in tokens)
                        Console.WriteLine($"{token.Id} - {token.Value} - {token.ApplicationClient.Name}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                throw;
            }
        }
    }
}